$('document').ready(function() {

    var theme = $.cookie('theme');
    changeTheme('/st/css/'+theme+'.css')
    $('#dark').bind( "click", function() {
        $.cookie('theme','dark');
        changeTheme('/st/css/dark.css')
    });

    $('#light').bind( "click", function() {
        $.cookie('theme','light');
        changeTheme('/st/css/light.css')
    });

    function changeTheme(theme_path) {
        var css_link=$('#theme');
        css_link.remove()
        css_link = $('<link id="theme" href="' + theme_path + '" id="dynamic-style-link" type="text/css" rel="stylesheet">');
        $('head').append(css_link);
        css_link.attr('href', theme_path);
    };
});