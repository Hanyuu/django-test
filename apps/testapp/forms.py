# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import ugettext as _

class UserNameForm(forms.Form):
   user_name = forms.CharField()

class UploadAvatar(forms.Form):
   file = forms.FileField()