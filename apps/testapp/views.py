# -*- coding: utf-8 -*-
from django.shortcuts import render,redirect
from django.shortcuts import render_to_response
from django.views.generic import View
from django.template import RequestContext
from .models import UserApp
from .forms import UserNameForm,UploadAvatar
from django.contrib.auth import logout
from django.utils.translation import activate, get_language,ugettext

class IndexView(View):

    form = UserNameForm

    def get(self, request):
        lang = request.session.get('lang')

        if lang :
            activate(lang)
        if not request.session.get('user'):
            return render_to_response('ask_user_name.html',{'form':self.form()}, RequestContext(request))
        else:
            user = request.session.get('user')

            return render_to_response('index.html', {'user': user}, RequestContext(request))


    def post(self,request):

        lang = request.session.get('lang')

        if lang:
            activate(lang)
        form = self.form(request.POST)

        if form.is_valid():
            user = None
            user_name = form.cleaned_data.get('user_name')
            try:
                user = UserApp.objects.get(name = user_name)

                request.session['user'] = user
            except:

                user = UserApp()

                user.name = user_name

                user.save()

                request.session['user'] = user

            return render_to_response('index.html', {'user': user}, RequestContext(request))
        else:

            return render_to_response('ask_user_name.html', {'form': self.form()}, RequestContext(request))


class LogOutView(View):
    form_user = UserNameForm
    def get(self,request):
        logout(request)
        return redirect('main_page')

class UploadAvatarView(View):

    form = UploadAvatar

    def get(self, request):
        lang = request.session.get('lang')
        if lang:
            activate(lang)
        user = request.session.get('user')
        if not user:
            return redirect('main_page')
        else:
            return render_to_response('upload_avatar.html',{'form':self.form(),'user':user},RequestContext(request))

    def post(self,request):
        lang = request.session.get('lang')
        user = request.session.get('user')
        if lang:
            activate(lang)
        form = self.form(request.POST, request.FILES)
        if form.is_valid():

            user.avatar = form.cleaned_data.get('file')
            user.save()
            return redirect('main_page')
        else:
            return render_to_response('upload_avatar.html', {'form': self.form(), 'user': user},RequestContext(request))

class SetLangEnView(View):

    def get(self,request):
        activate('en')
        request.session['lang'] = 'en';
        return redirect('main_page')

class SetLangRuView(View):

    def get(self,request):
        activate('ru')
        request.session['lang'] = 'ru';
        return redirect('main_page')