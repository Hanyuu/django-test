# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import os
from django.conf import settings

avatar = 'users/%s/%s'

def upload_to_avatar(instance, filename):
    """
    Загружает аватар пользователя в папку users.
    """
    _avatar = avatar % (instance.pk, 'avatar.jpg')
    if os.path.isfile('%s/%s'%(settings.MEDIA_ROOT,_avatar)):
        os.remove('%s/%s'%(settings.MEDIA_ROOT,_avatar))
    return _avatar

class UserApp(models.Model):

    name = models.CharField(u'Имя',max_length=100,blank=False,null=False)

    avatar = models.ImageField(u'Аватар',upload_to=upload_to_avatar,null=True,blank=True)

    def __str__(self):
        return u'%s'%self.name
