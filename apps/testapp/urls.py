# -*- coding: utf-8 -*-
from django.conf.urls import url
from .views import IndexView, LogOutView, UploadAvatarView,SetLangEnView,SetLangRuView
from django.utils.translation import ugettext_lazy as _
urlpatterns = (

    url(r'^$',IndexView.as_view(),name='main_page'),

    url(r'^logout/$',LogOutView.as_view(),name='log_out_page'),

    url(r'^upload/$',UploadAvatarView.as_view(),name='upload_avatar_page'),

    url(r'^en/$',SetLangEnView.as_view(),name='set_lang_en'),

    url(r'^ru/$',SetLangRuView.as_view(),name='set_lang_ru'),
)