from django.contrib import admin

# Register your models here.
from .models import UserApp

class UserAppAdmin(admin.ModelAdmin):
    pass
admin.site.register(UserApp,UserAppAdmin)